module InputFormatterHelper
  class Base
    def initialize(fixture:)
      @raw_input = File.read("./spec/#{fixture}")
    end
  end

  class CaloriesCounter < Base
    def read
      items_per_elf = @raw_input.split("\n\n")
      items_per_elf.map do |item|
        item.split.map(&:to_i)
      end
    end
  end

  class RockPaperScissors < Base
    def read
      rounds = @raw_input.split("\n").map(&:split)
    end
  end
end
