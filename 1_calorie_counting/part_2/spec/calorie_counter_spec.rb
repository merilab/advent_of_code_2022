require_relative '../../../spec/spec_helper'
require_relative '../lib/calorie_counter'

RSpec.describe CalorieCounter::Part2::CalorieCounter do
  it 'calculates the higher number of calories' do
    calories_per_elf = InputFormatterHelper::CaloriesCounter.new(fixture: '../1_calorie_counting/part_2/spec/fixtures/input_1').read
    calorie_counter = described_class.new(calories_per_elf: calories_per_elf)

    result = calorie_counter.run

    expect(result).to eq(45000)
  end

  it 'calculates the higher number of calories' do
    calories_per_elf = InputFormatterHelper::CaloriesCounter.new(fixture: '../1_calorie_counting/part_2/spec/fixtures/input_2').read
    calorie_counter = described_class.new(calories_per_elf: calories_per_elf)

    result = calorie_counter.run

    expect(result).to eq(212117)
  end
end
