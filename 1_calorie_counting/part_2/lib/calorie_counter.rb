module CalorieCounter
  module Part2
    class CalorieCounter
      def initialize(calories_per_elf:)
        @calories_per_elf = calories_per_elf
      end

      def run
        @calories_per_elf.map(&:sum).sort.last(3).sum
      end
    end
  end
end
