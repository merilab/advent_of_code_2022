module CalorieCounter
  module Part1
    class CalorieCounter
      def initialize(calories_per_elf:)
        @calories_per_elf = calories_per_elf
      end

      def run
        @calories_per_elf.map(&:sum).max
      end
    end
  end
end
