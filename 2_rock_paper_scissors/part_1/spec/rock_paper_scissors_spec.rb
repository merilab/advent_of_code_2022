require_relative '../../../spec/spec_helper'
require_relative '../lib/rock_paper_scissors'

RSpec.describe RockPaperScissors::Part1::RockPaperScissors do
  it 'calculates the higher number of calories' do
    rounds = InputFormatterHelper::RockPaperScissors.new(fixture: '../2_rock_paper_scissors/part_1/spec/fixtures/input_1').read
    service = described_class.new(rounds: rounds)

    result = service.run

    expect(result).to eq(15)
  end

  it 'calculates the higher number of calories' do
    rounds = InputFormatterHelper::RockPaperScissors.new(fixture: '../2_rock_paper_scissors/part_1/spec/fixtures/input_2').read
    service = described_class.new(rounds: rounds)

    result = service.run

    expect(result).to eq(11449)
  end
end
