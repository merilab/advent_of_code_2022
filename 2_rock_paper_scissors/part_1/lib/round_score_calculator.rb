module RockPaperScissors
  module Part1
    class RoundScoreCalculator
      DRAW_SCORE = 3
      LOSE_SCORE = 0
      WIN_SCORE = 6

      SHAPES_SPECS = {
        rock: { beats: :scissors, value: 1 },
        scissors: { beats: :paper, value: 3 },
        paper: { beats: :rock, value: 2 }
      }

      SHAPES_UNCODER = {
        'A' => :rock,
        'B' => :paper,
        'C' => :scissors,
        'X' => :rock,
        'Y' => :paper,
        'Z' => :scissors
      }

      def initialize(shapes:)
        @their = uncode(shapes.first)
        @mine = uncode(shapes.last)
      end

      def run
        shape_value = SHAPES_SPECS[@mine][:value]
        game_score = deduce_game_score

        score_for(game_score, shape_value)
      end

      private

      def deduce_game_score
        return DRAW_SCORE if draw?
        return LOSE_SCORE if SHAPES_SPECS[@their][:beats] == @mine

        WIN_SCORE
      end

      def draw?
        @their == @mine
      end

      def lose?
        SHAPES_SPECS[@their][:beats] == @mine
      end

      def score_for(result_score, shape_value)
        result_score + shape_value
      end

      def uncode(shape_code)
        SHAPES_UNCODER[shape_code]
      end
    end
  end
end
