module RockPaperScissors
  module Part2
    class RoundScoreCalculator
      DRAW_SCORE = 3
      LOSE_SCORE = 0
      WIN_SCORE = 6

      STRATEGY_UNCODER = {
        'X' => :lose,
        'Y' => :draw,
        'Z' => :win
      }

      SHAPES_SPECS = {
        rock: { beaten_by: :paper, beats: :scissors, value: 1 },
        scissors: { beaten_by: :rock, beats: :paper, value: 3 },
        paper: { beaten_by: :scissors, beats: :rock, value: 2 }
      }

      SHAPES_UNCODER = {
        'A' => :rock,
        'B' => :paper,
        'C' => :scissors
      }

      def initialize(shapes:)
        @their = SHAPES_UNCODER[shapes.first]
        @strategy = STRATEGY_UNCODER[shapes.last]
      end

      def run
        score_for(game_score, shape_value)
      end

      private

      def game_score
        return DRAW_SCORE if @strategy == :draw
        return LOSE_SCORE if @strategy == :lose

        WIN_SCORE
      end

      def score_for(result_score, shape_value)
        result_score + shape_value
      end

      def shape_value
        return SHAPES_SPECS[@their][:value] if @strategy == :draw
        return SHAPES_SPECS[SHAPES_SPECS[@their][:beats]][:value] if @strategy == :lose

        SHAPES_SPECS[SHAPES_SPECS[@their][:beaten_by]][:value]
      end
    end
  end
end
