require_relative './round_score_calculator'

module RockPaperScissors
  module Part2
    class RockPaperScissors
      def initialize(rounds:)
        @rounds = rounds
      end

      def run
        @rounds.sum { |round| RoundScoreCalculator.new(shapes: round).run }
      end
    end
  end
end
