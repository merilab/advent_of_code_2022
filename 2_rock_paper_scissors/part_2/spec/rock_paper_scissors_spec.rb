require_relative '../../../spec/spec_helper'
require_relative '../lib/rock_paper_scissors'

RSpec.describe RockPaperScissors::Part2::RockPaperScissors do
  it 'calculates the higher number of calories' do
    rounds = InputFormatterHelper::RockPaperScissors.new(fixture: '../2_rock_paper_scissors/part_2/spec/fixtures/input_1').read
    calorie_counter = described_class.new(rounds: rounds)

    result = calorie_counter.run

    expect(result).to eq(12)
  end

  it 'calculates the higher number of calories' do
    rounds = InputFormatterHelper::RockPaperScissors.new(fixture: '../2_rock_paper_scissors/part_2/spec/fixtures/input_2').read
    calorie_counter = described_class.new(rounds: rounds)

    result = calorie_counter.run

    expect(result).to eq(13187)
  end
end
