require_relative '../../../spec/spec_helper'
require_relative '../lib/round_score_calculator'

RSpec.describe RockPaperScissors::Part2::RoundScoreCalculator do
  it 'calculates the higher number of calories' do
    shapes = ['A', 'Y']
    score_calculator = described_class.new(shapes: shapes)

    result = score_calculator.run

    expect(result).to eq(4)
  end

  it 'calculates the higher number of calories' do
    shapes = ['B', 'X']
    score_calculator = described_class.new(shapes: shapes)

    result = score_calculator.run

    expect(result).to eq(1)
  end

  it 'calculates the higher number of calories' do
    shapes = ['C', 'Z']
    score_calculator = described_class.new(shapes: shapes)

    result = score_calculator.run

    expect(result).to eq(7)
  end
end
